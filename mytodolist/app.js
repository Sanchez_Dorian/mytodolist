console.log("Hello World");

var AddTaskButton = document.getElementById("addTaskButton");

var InputTaskToDo = document.getElementById("inputTaskToDo");

var OptionButton = document.getElementById("optionButton");

var MenuSortTasks = document.getElementById("MenuSelectTasks");

var DivSortAll = document.getElementById("SortAll");

var DivSortDone = document.getElementById("SortDone");

var DivSortToDo = document.getElementById("SortToDo");

var TaskToDo = document.getElementsByClassName("ToDo");
var TaskDone = document.getElementsByClassName("Done");


var ContainerAll = document.getElementById("TasksToDo");
var ContainerArticle = document.getElementById("Task");

AddTaskButton.addEventListener("click", function()
	{

		if (InputTaskToDo.value != "" && InputTaskToDo.value.length <= 25)
		{

			var ContainerArticle = document.createElement("article");
			ContainerArticle.classList.add("Task");
			ContainerArticle.classList.add("ToDo");
			ContainerAll.appendChild(ContainerArticle);

			var NewTaskText = document.createElement("p");

			NewTaskText.classList.add("TaskTxt");
			ContainerArticle.appendChild(NewTaskText);
			NewTaskText.innerText = InputTaskToDo.value;
			
			var DivAllButton = document.createElement("div");
			DivAllButton.classList.add("ButtonsTasks");
			ContainerArticle.appendChild(DivAllButton);

			var DivButtonDone = document.createElement("button");
			DivButtonDone.classList.add("DoneTaskBox");
			DivAllButton.appendChild(DivButtonDone );

			var DivButtonDelete = document.createElement("button");
			DivButtonDelete.classList.add("DeleteTaskButton");
			DivAllButton.appendChild(DivButtonDelete);

			InputTaskToDo.value = "";
		}

		DivButtonDone.addEventListener("click", function()
			{

				NewTaskText.style.textDecoration = "line-through";
				ContainerArticle.classList.add("Done");
				ContainerArticle.classList.remove("ToDo");
				//ContainerArticle.classList.add("Hidden");

			});

		DivButtonDelete.addEventListener("click", function()
			{
				ContainerAll.removeChild(ContainerArticle);
			});
	});


		DivSortAll.addEventListener("click", function()
			{
				ContainerArticle.classList.remove("Hidden");
			});

		DivSortDone.addEventListener("click", function()
			{
				TaskDone.classList.remove("Hidden");
				TaskToDo.classList.add("Hidden");
			});

		DivSortToDo.addEventListener("click", function()
			{
				TaskToDo.classList.remove("Hidden");
				TaskDone.classList.add("Hidden");
			});